# scanduplexer

Did you ever wish you could turn your single-side scanner to duplex scanner?

Scan your pile of documents on one side, then the other, and use scanduplexer to combine your single side scans as if you had a duplex scanner!

scanduplexer provides a simple graphical user interface (GUI) to select front and back PDFs or images, and interleaves pages for either pdfs or images. The combined files are saved in a specified output directory.

![scanduplexer image](https://gitlab.com/uploads/-/system/personal_snippet/3720724/332b0ff7d0ceaee47ee282eb26ae4eae/scan_duplexer.png)

## Features

- Interleave front and back scan for PDF pages, or images.
- User-friendly GUI

## Requirements

- Python 3.6+
- Tkinter (usually included with Python)
- Pillow (Python Imaging Library)
- PyPDF2
- reportlab (for generating test PDFs)

## Installation

1. **Clone the repository**:
   ```bash
   git clone https://gitlab.com/edlee123/scanduplexer
   cd scanduplexer
   ```
   
2.  **Install package:**:
    ```bash
    pip install .
    ```

## Usage

### Generating Test PDFs

Can use the provided script to generate test PDFs:

```bash
generate_mock_pdfs <target path>
```

This will create two test PDF files (front.pdf and back.pdf) each containing 5 pages in the mock_pdfs directory.

### Run the Scanduplexer application:

```bash
scanduplexer
```

Combining Files

    Select Mode: Choose between "Image Mode" and "PDF Mode" using the radio buttons.
    Select Front Files:
        For PDF Mode: Click "Select Front Files" and choose a single PDF file.
        For Image Mode: Click "Select Front Files" and choose multiple image files.
    Select Back Files:
        For PDF Mode: Click "Select Back Files" and choose a single PDF file.
        For Image Mode: Click "Select Back Files" and choose multiple image files.
    Select Output Directory: Click "Select Output Directory" and choose where the combined files will be saved.
    Combine Files: Click "Combine Files" to start the process. A success message will be displayed when the process is complete.

Notes

    In PDF Mode, the number of pages in the front and back PDFs must be the same.
    In Image Mode, the number of front and back images must be the same.


## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.
