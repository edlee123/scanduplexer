from reportlab.lib.pagesizes import letter
from reportlab.pdfgen import canvas
import argparse
import os


def create_test_pdf(filename, num_pages, odd=None, reversed=False):
    c = canvas.Canvas(filename, pagesize=letter)
    width, height = letter

    for i in range(1, num_pages + 1):
        if reversed:
            page_index = num_pages + 1 - i
        else:
            page_index = i

        if odd:
            page_num = 2 * page_index - 1
        elif odd is False:
            page_num = 2 * page_index
        else:
            page_num = i
        c.drawString(100, height - 100, f"This is page {page_num}")
        c.showPage()
    c.save()


def main():
    parser = argparse.ArgumentParser(description="Generate test PDF files.")
    parser.add_argument("output_path", type=str, help="The output directory for the test PDFs.")
    args = parser.parse_args()

    # Define the number of pages for the front and back PDFs
    num_front_pages = 5
    num_back_pages = 5

    # Create the output directory if it doesn't exist
    os.makedirs(args.output_path, exist_ok=True)

    # Create front and back PDF files
    front_pdf_path = os.path.join(args.output_path, "front.pdf")
    back_pdf_path = os.path.join(args.output_path, "back.pdf")

    # Generate the PDFs
    create_test_pdf(front_pdf_path, num_front_pages)
    create_test_pdf(back_pdf_path, num_back_pages, odd=False, reversed=True)

    print(f"Test PDFs created:\n{front_pdf_path}\n{back_pdf_path}")


if __name__ == "__main__":
    main()
