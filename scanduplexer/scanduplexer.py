import os
from PIL import Image
import tkinter as tk
from tkinter import filedialog, messagebox
from PyPDF2 import PdfReader, PdfWriter


# Function to combine two images vertically
def combine_images(front_image_path, back_image_path, output_path):
    front_image = Image.open(front_image_path)
    back_image = Image.open(back_image_path)
    front_width, front_height = front_image.size
    back_width, back_height = back_image.size
    combined_image = Image.new('RGB', (front_width, front_height + back_height))
    combined_image.paste(front_image, (0, 0))
    combined_image.paste(back_image, (0, front_height))
    combined_image.save(output_path)


# Function to combine PDF pages
def combine_pdfs(front_pdf_path, back_pdf_path, output_path):
    # This function assumes scanner reverse pages
    front_pdf = PdfReader(front_pdf_path)
    back_pdf = PdfReader(back_pdf_path)
    if len(front_pdf.pages) != len(back_pdf.pages):
        messagebox.showerror("Error", "The number of front and back PDF pages must be the same.")
        return
    output_pdf = PdfWriter()
    # This function assumes scanner reverse pages, therefore reverses the back pages
    for front_page, back_page in zip(front_pdf.pages, reversed(back_pdf.pages)):
        output_pdf.add_page(front_page)
        output_pdf.add_page(back_page)
    with open(output_path, 'wb') as f:
        output_pdf.write(f)
    messagebox.showinfo("Success", "PDFs have been combined and saved in the output directory.")


# Function to select files for the front set
def select_front_files():
    global front_files, front_pages_count
    if mode.get() == "image":
        front_files = filedialog.askopenfilenames(title="Select Front Images",
                                                  filetypes=[("Image files", "*.jpg *.jpeg *.png")])
        front_pages_count = len(front_files)
    else:
        front_files = [filedialog.askopenfilename(title="Select Front PDF", filetypes=[("PDF files", "*.pdf")])]
        if front_files[0]:
            front_pdf = PdfReader(front_files[0])
            front_pages_count = len(front_pdf.pages)
        else:
            front_pages_count = 0
    front_label.config(text=f"Selected {front_pages_count} front pages.")


# Function to select files for the back set
def select_back_files():
    global back_files, back_pages_count
    if mode.get() == "image":
        back_files = filedialog.askopenfilenames(title="Select Back Images",
                                                 filetypes=[("Image files", "*.jpg *.jpeg *.png")])
        back_pages_count = len(back_files)
    else:
        back_files = [filedialog.askopenfilename(title="Select Back PDF", filetypes=[("PDF files", "*.pdf")])]
        if back_files[0]:
            back_pdf = PdfReader(back_files[0])
            back_pages_count = len(back_pdf.pages)
        else:
            back_pages_count = 0
    back_label.config(text=f"Selected {back_pages_count} back pages.")


# Function to select output directory
def select_output_directory():
    global output_directory
    output_directory = filedialog.askdirectory(title="Select Output Directory")
    output_label.config(text=f"Output directory: {output_directory}")


# Function to combine selected files
def combine_selected_files():
    if not front_files or not back_files:
        messagebox.showerror("Error", "Please select both front and back files.")
        return

    if mode.get() == "image":
        if len(front_files) != len(back_files):
            messagebox.showerror("Error", "The number of front and back images must be the same.")
            return

        if not output_directory:
            messagebox.showerror("Error", "Please select an output directory.")
            return

        reversed_back_files = list(reversed(back_files))
        for front_image_path, back_image_path in zip(front_files, reversed_back_files):
            front_image_name = os.path.basename(front_image_path)
            output_image_path = os.path.join(output_directory, f'combined_{front_image_name}')
            combine_images(front_image_path, back_image_path, output_image_path)
        messagebox.showinfo("Success", "Images have been combined and saved in the output directory.")

    else:
        if not output_directory:
            messagebox.showerror("Error", "Please select an output directory.")
            return

        front_pdf_path = front_files[0]
        back_pdf_path = back_files[0]
        file_name = os.path.basename(front_pdf_path)
        file_name_no_ext = os.path.splitext(file_name)[0]
        output_pdf_path = os.path.join(output_directory, f"{file_name_no_ext}_combined_output.pdf")
        combine_pdfs(front_pdf_path, back_pdf_path, output_pdf_path)


# Function to handle window close event
def on_closing():
    if messagebox.askokcancel("Quit", "Do you want to quit?"):
        root.destroy()


# Create the main window
root = tk.Tk()
root.title("Scan Duplexer")

# Handle window close event
root.protocol("WM_DELETE_WINDOW", on_closing)

# Variables to store selected files and output directory
front_files = []
back_files = []
front_pages_count = 0
back_pages_count = 0
output_directory = ""
mode = tk.StringVar(value="image")

# Create and place widgets
tk.Radiobutton(root, text="Image Mode", variable=mode, value="image").pack(anchor=tk.W, padx=20)
tk.Radiobutton(root, text="PDF Mode", variable=mode, value="pdf").pack(anchor=tk.W, padx=20)

tk.Button(root, text="Select Front Files", command=select_front_files).pack(pady=5)
front_label = tk.Label(root, text="No front files selected.")
front_label.pack(pady=5)

tk.Button(root, text="Select Back Files", command=select_back_files).pack(pady=5)
back_label = tk.Label(root, text="No back files selected.")
back_label.pack(pady=5)

tk.Button(root, text="Select Output Directory", command=select_output_directory).pack(pady=5)
output_label = tk.Label(root, text="No output directory selected.")
output_label.pack(pady=5)

tk.Button(root, text="Combine Files", command=combine_selected_files).pack(pady=20)

# Run the main loop
root.mainloop()
