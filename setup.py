from setuptools import setup, find_packages

setup(
    name='scanduplexer',
    version='0.1.0',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'Pillow',
        'PyPDF2',
        'reportlab'
    ],
    entry_points={
        'console_scripts': [
            'scanduplexer = scanduplexer.scanduplexer:main',
            'generate_mock_pdfs = scanduplexer.generate_mock_pdfs:main'
        ],
    },
    author='Ed Lee',
    author_email='edwardmtlee@gmail.com',
    description='A tool to combine front and back scanned images or PDFs into a single file.',
    long_description=open('README.md').read(),
    long_description_content_type='text/markdown',
    url='https://github.com/yourusername/scanduplexer',
    classifiers=[
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
    ],
    python_requires='>=3.6',
)
